-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2015 at 08:54 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `staff_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `active`) VALUES
(1, 'PHP', 1),
(2, '.NET', 1),
(3, 'Mobile', 1),
(4, 'Game', 1),
(5, '3D', 1),
(6, '', 1),
(7, 'Cert', 1),
(8, 'Department Test', 0),
(9, 'Department Test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `name`, `role_id`, `active`) VALUES
(1, 'Junior', 4, 1),
(2, 'Senior', 4, 1),
(3, 'Leader', 3, 1),
(4, 'Manager 1', 2, 1),
(5, 'Manager 2', 2, 1),
(6, 'SystemAdmin', 1, 1),
(8, 'Leader Test', 3, 1),
(9, 'Manager Test', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_14_080016_create_staff_table', 1),
('2015_11_14_080246_create_system_admins_table', 2),
('2015_11_14_080535_create_reviews_table', 3),
('2015_11_14_080938_create_departments_table', 4),
('2015_11_14_081030_create_levels_table', 5),
('2015_11_17_031511_create_positions_table', 6),
('2015_11_17_100817_rename_colnum_staff', 7),
('2015_11_18_031453_add_colnum_remember_systempadmin', 8);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `name`, `active`) VALUES
(1, 'Backend', 1),
(2, 'Fontend', 1),
(3, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `staff_id`, `reviewer_id`, `point`, `comment`, `active`, `created_at`, `updated_at`) VALUES
(12, 32, 28, 6, 'good', 1, '2015-12-09 02:09:09', '2015-12-09 02:09:09'),
(13, 35, 28, 7, 'good', 1, '2015-12-09 02:09:24', '2015-12-09 02:09:24'),
(14, 30, 28, 7, 'good', 1, '2015-12-09 02:09:34', '2015-12-09 02:09:34'),
(15, 31, 28, 6, 'good', 1, '2015-12-09 02:09:47', '2015-12-09 02:09:47'),
(16, 34, 28, 8, 'good', 1, '2015-12-09 02:10:07', '2015-12-09 02:10:07'),
(17, 24, 25, 6, 'Good', 1, '2015-12-09 02:48:25', '2015-12-09 02:48:25'),
(18, 18, 25, 7, 'good', 1, '2015-12-09 02:48:38', '2015-12-09 02:48:38'),
(19, 29, 25, 5, '', 1, '2015-12-09 02:48:53', '2015-12-09 02:48:53'),
(20, 29, 25, 7, '', 1, '2015-12-09 02:48:57', '2015-12-09 02:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `active`) VALUES
(1, 'SystemAdmin', 1),
(2, 'Manager', 1),
(3, 'TeamLeader', 1),
(4, 'Developer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `position_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `email`, `password`, `name`, `phone`, `birthday`, `position_id`, `level_id`, `department_id`, `active`, `remember_token`) VALUES
(1, 'dinhtich91@gmail.com', '$2y$10$jCmonR0X/4H6aQgOlTdIJO9Txy0.kcLWqAdL1jdf.7M4CzHLi8aBa', 'Tạ Đình Tích', '972571744', '1991-12-10', 1, 4, 6, 1, 'aI1eT4HNTwmZcX28kdFz0JbcFsnjdHr25MMqtVPhWRpaaimbVMAbEpVwUiIf'),
(18, 'hieulucky111@gmail.com', '$2y$10$kKvBSXYuytvA4AQODPAOIe7YCQRvdNWyuQ67lH3QV/MMcdbrCyI3W', 'Võ Minh Bảo Hiếu', '0965602322', '1993-12-06', 2, 1, 1, 1, 'Nw4TeBBUh3JyNZrPCLip6RdOjA2MUD8iZRtph9voPN7EOQmnoFXY3UMUJb68'),
(20, 'SystemAdmin@admin.com', '$2y$10$kKvBSXYuytvA4AQODPAOIe7YCQRvdNWyuQ67lH3QV/MMcdbrCyI3W', 'SystemAdmin', '', '0000-00-00', 3, 6, 6, 1, 'LJ1vFtsVZSeguR4ijF0v7SYKYb2gAoQgAxhMKYved7O7iqkMCWbeNlwCrXGu'),
(21, 'test@test.com', '$2y$10$P9cciUuBiOJ0vc7ySqkWEOX0n6jOPVFuJVeNS1D2WeMUocpbNTe2.', 'a', '1234567890', '2015-12-31', 1, 4, 6, 1, ''),
(22, 'test@thu.com', '$2y$10$zaTZSgK1OzHMWG297wqvCe7YTwZFOujlP4OP6H4DPi225sZrVudLq', 'q', '1234567890', '2015-12-29', 3, 4, 6, 1, ''),
(23, 'Manager@manager.com', '$2y$10$bewSyp/eENHweBUf18FHfuUh3x5JenMGYMYLfA/99p4VwEe/LOGkO', 'Manager', '1234567890', '2015-12-30', 3, 4, 6, 1, 'JEoz7MJjnI0xfcGFx73pfQA9qQ4GpYuwLRjN4IwE38SE1PssHuYTV1b8bb7J'),
(24, 'testmanager@test.com', '$2y$10$kcACP7dIQ4MdkKeXjJGAYeTn.KtT39ZBFA.kHnQUEaCMraXrr9igm', 'aaaaaa', '1234567890', '2015-12-29', 1, 3, 1, 1, ''),
(25, 'Leader@leader.com', '$2y$10$jUzPWVLl07jEg0I8uaRTyufAEHHFQC7DjiCzEW0GkbuYWU8VdGRLq', 'Leader', '1234567890', '2015-12-31', 1, 3, 1, 1, 'NkiBzfOMfgeAiRK3qYpUP0Z8IoD5RJcDESKoHc4vvYQzDeGd0DJ32gSF3fap'),
(28, 'managertest@manager.com', '$2y$10$0r.HLfXGczGpPRm4csmvJ.W7XrnMOgXCN7E.7Op9U/z6xxzjemqia', 'Manager Test', '1234567890', '2015-12-31', 3, 9, 6, 1, 'EK1LXWrwvgUEEMsI75umVKYyXB7Te5q6SzFLHIZJAfhblrLB0ayiyI0T5Hkc'),
(29, 'leadertest@leader.com', '$2y$10$ny.MvYdNm.td5mYoZdiGDetSk25/.oS.zL2qQRbQ9iLwSwbpObmaC', 'Leader Test', '1234567890', '2015-12-30', 1, 1, 1, 1, ''),
(30, 'dev1@dev.com', '$2y$10$IB6l9KUz/GrvLLwrD6Cf1O3yxigdaOxIQ8KWGR8kMOVdnThaI.Y9O', 'dev1', '1234567890', '2015-12-23', 1, 1, 7, 1, 'PRQ3AZwV4I8yOM9YLgSysqhhShHQ2tYozS9Qr6R3hYSnVdcryeDZU7JE9CTM'),
(31, 'dev2@dev.com', '$2y$10$gfIVF.6NxfasLWg/YKlRiebgD9gM.Q1Kw7FSxad7mNDHXi3tPYwRy', 'dev2', '1234567890', '2015-12-15', 1, 1, 5, 1, ''),
(32, 'dev3@dev.com', '$2y$10$/V4CqWLvU6NnnC9LBTmicOVOQwjpgDIVDA0CuLfiTDnYtjMogmR2a', 'dev3', '1234567890', '2015-12-30', 1, 1, 1, 1, ''),
(33, 'dev4@dev.com', '$2y$10$UBZLcBrxGf4VfEl4159BjOagrsgZ8RZvvlyXR7XLgB1pwB0RnHibi', 'dev4', '1234567890', '2015-12-30', 1, 1, 4, 1, ''),
(34, 'dev5@dev.com', '$2y$10$1gL91cohOeZt71euoF7gmOgjguDYL47omFr/bq61sNYmD7kyRkn6u', 'dev5', '1234567890', '2015-12-31', 1, 1, 3, 1, ''),
(35, 'dev6@dev.com', '$2y$10$mF5Z0CqfODiqlI/.FZfac.thyBl226v9gGSv6YVfaejiYSGf./tkO', 'dev6', '1234567890', '2015-12-31', 1, 1, 2, 1, ''),
(36, 'dev7@dev.com', '$2y$10$20PYSB4q3A8Dinlj9fsI/.xLS4lcyAIOyvC7cxJFh/3dge55NDXL2', 'dev7', '1234567890', '2015-12-25', 1, 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `staff_team`
--

CREATE TABLE IF NOT EXISTS `staff_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `staff_team`
--

INSERT INTO `staff_team` (`id`, `staff_id`, `team_id`) VALUES
(52, 28, 20),
(53, 23, 20),
(54, 24, 20),
(59, 18, 21),
(60, 32, 21),
(61, 36, 21),
(63, 18, 22),
(64, 29, 22),
(65, 32, 22),
(66, 36, 22),
(68, 18, 23),
(69, 32, 23),
(70, 25, 24),
(71, 32, 24),
(72, 36, 24),
(73, 25, 25);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `type_id`, `active`) VALUES
(20, 'Manager Team Test', '1', 1),
(21, 'Team Dev Test', '2', 1),
(22, 'Team Dev 2', '2', 1),
(23, 'Team Dev 3', '2', 1),
(24, 'Developer Team ', '2', 1),
(25, 'Developer Team Edit', '2', 1),
(26, 'Developer Team test', '2', 1),
(27, 'Team Dev Test', '2', 1),
(28, 'Team Dev 1', '2', 1),
(29, 'Team Dev 1', '2', 1),
(30, 'Team Dev 1', '2', 1),
(31, 'Team Dev 1', '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`) VALUES
(1, 'Manager'),
(2, 'Developer');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
