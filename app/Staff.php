<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Staff extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    protected 	$table 		=	'staff';
    protected   $hidden     =   ['password', 'remember_token'];
    protected 	$fillable 	=	[ 'email', 'password', 'name', 'phone', 'birthday', 'position_id', 'level_id', 'department_id', 'active' ];
    public 		$timestamps =	false;

    public function level()
    {
    	return $this->belongsTo( 'App\Level', 'level_id' );
    }
    public function department()
    {
    	return $this->belongsTo( 'App\Department', 'department_id' );
    }
    public function position()
    {
    	return $this->belongsTo( 'App\Position', 'position_id' );
    }
    public function review()
    {
    	return $this->hasMany( 'App\Review', 'staff_id' );
    }
}
