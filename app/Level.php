<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected 	$table 		=	'level';
    protected 	$fillable	=	[ 'name', 'role_id', 'active' ];
    public 		$timestamps =	false;	
    public function role()
    {
    	return $this->belongsTo( 'App\Role', 'role_id' );
    }
}
