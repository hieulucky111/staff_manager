<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Department;
use Gate;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.master',function($view){
            $department=Department::where( 'id', '!=', 6 )->where( 'active', 1 )->get();
            if ( Gate::allows('team_leader') )
            {
                $department=Department::where( 'id', Auth::user()->department_id )->where( 'active', 1 )->get();
            }
            if ( Gate::allows('developer') )
            {
                $department=Department::where( 'id', Auth::user()->department_id )->where( 'active', 1 )->get();
            }
            $view->with('department',$department);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
