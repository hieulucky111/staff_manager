<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Level;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);


        //Role SystemAdmin
        $gate->define( 'system_admin', function ($user)
        {
            $check = $user->level->role_id;
            if( $check == '1' ) {
                return true;
            } else {
                return false;
            }
        } );

        //Role Manager
        $gate->define( 'manager', function ($user)
        {
            $check = $user->level->role_id;
            if( $check == '2' ) {
                return true;
            } else {
                return false;
            }
        } );

        //Role Team Leader
        $gate->define( 'team_leader', function ($user)
        {
            $check = $user->level->role_id;
            if( $check == '3' ) {
                return true;
            } else {
                return false;
            }
        } );

        //Role Developer
        $gate->define( 'developer', function ($user)
        {
            $check = $user->level->role_id;
            if( $check == '4' ) {
                return true;
            } else {
                return false;
            }
        } );

        //Permission create staff
        $gate->define( 'role_create', function ($user, $new_staff)
        {
            $check = $user->level->role_id;
            if( $check == '1' ) {
                if( $new_staff == '2' || $new_staff == '3' ) {
                    return true;
                } else {
                    return false;
                }
            }
            elseif( $check == '2' ) {
                if( $new_staff == '3' || $new_staff == '4' ) {
                    return true;
                } else {
                    return false;
                }
            }
            elseif( $check == '3' ) {
                if( $new_staff == '4' ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } );
    }
}
