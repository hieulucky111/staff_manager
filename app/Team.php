<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected 	$table 		=	'team';
    protected 	$fillable	=	[ 'name', 'type_id', 'active' ];
    public 		$timestamps =	false;	
    public function type()
    {
    	return $this->belongsTo( 'App\Type', 'type_id' );
    }
}
