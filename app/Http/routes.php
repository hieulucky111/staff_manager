<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route login
Route::get( '/', [ 'middleware' => 'guest', 'as' => 'getLogin', 'uses' => 'Auth\AuthController@getLogin' ] );
Route::post( '/', [ 'middleware' => 'guest', 'as' => 'postLogin', 'uses' => 'Auth\AuthController@postLogin' ] );
//Route logout
Route::get( 'logout', [ 'as' => 'getLogout', 'uses' => 'Auth\AuthController@getLogout'] );
//Route Dashboard
Route::get( 'dashboard', [ 'middleware' => 'auth', 'as' => 'getDashboard', 'uses' => 'DashboardController@get_dashboard' ]);
//Route staff
Route::resource( 'staff', 'StaffController' );
//Route department
Route::resource( 'department', 'DepartmentController' );
//Route review
Route::resource( 'review', 'ReviewController' );
//Route level
Route::resource( 'level', 'LevelController' );
//Route team
Route::resource( 'team', 'TeamController' );
//Route export Excel file
Route::get( 'excel', [ 'as' => 'exportExcel', 'uses' => 'ExportController@export_excel' ] );
//Route delete staff belong a team
Route::get( 'delete/{id}', [ 'middleware' => 'auth', 'as' => 'deleteStaffTeam', 'uses' => 'DeleteController@staff_team_delete' ] );
//Route edit user information 
Route::get( 'profile', [ 'middleware' => 'auth', 'as' => 'getProfile', 'uses' => 'ProfileController@get_profile' ] );
Route::post( 'profile', [ 'middleware' => 'auth', 'as' => 'postProfile', 'uses' => 'ProfileController@post_profile' ] );