<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'  =>  'required|min:8|max:16',
            'name'      =>  'required',
            'phone'     =>  'required|min:10|max:11',
            'birthday'  =>  'required',
        ];
    }
    public function messages() 
    {
        return [
            'password.required' =>  'Password is required',
            'password.min'      =>  'Password is 8-16',
            'password.max'      =>  'Password is 8-16',
            'name.required'     =>  'Name is required',
            'phone.required'    =>  'Phone is required',
            'phone.min'         =>  'Phone is 10-11 character',
            'phone.max'         =>  'Phone is 10-11 character',
            'birthday.required' =>  'Birthday is required',
        ];
    }
}
