<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateStaffRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required',
            'phone'     =>  'required|min:10|max:11',
            'birthday'  =>  'required',
        ];
    }
    public function messages() 
    {
        return [
            'name.required'     =>  'Name is required',
            'phone.required'    =>  'Phone is required',
            'phone.min'         =>  'Phone is 10-11 character',
            'phone.max'         =>  'Phone is 10-11 character',
            'birthday.required' =>  'Birthday is required',
        ];
    }
}
