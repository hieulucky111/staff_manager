<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Team;
use App\Http\Requests\AddTeamRequest;
use App\StaffTeam;
use App\Staff;
use Auth;
use Gate;
use App\Level;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	if ( Gate::allows( 'developer' ) )
    	{
    		return view('admin.error403');
    	}
    	if ( Gate::denies( 'system_admin' ) )
    	{
            if ( Gate::allows( 'manager' ) )
            {
    		    $team = Team::where( 'active', 1 )
                            ->where( 'type_id', 1 )
    		    			->whereIn( 'id', StaffTeam::select( 'team_id' )->where( 'staff_id', Auth::user()->id )->get() )
    		    			->get();
            }
            if ( Gate::allows( 'team_leader' ) )
            {
                $team = Team::where( 'active', 1 )
                            ->where( 'type_id', 2 )
                            ->whereIn( 'id', StaffTeam::select( 'team_id' )->where( 'staff_id', Auth::user()->id )->get() )
                            ->get();
            }	        
		    return view('admin.team.list', compact('team'));
	    } else {
	    	return view('admin.error403');
	    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
    		if ( Gate::allows('manager') )
    		{
		        $staff  = Staff::where( 'active', 1 )
		        				->where( 'id', '!=', Auth::user()->id )
		        				->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', 2 )->orWhere( 'role_id', 3 )->get() )
		        				->get();		        
			}
			if ( Gate::allows('team_leader') )
    		{
		        $staff  = Staff::where( 'active', 1 )
		        				->where( 'id', '!=', Auth::user()->id )
		        				->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', 4 )->get() )
		        				->where( 'department_id', Auth::user()->department_id )
		        				->get();		        
			}
			if ( Gate::allows('developer') )
			{
				return view('admin.error403');
			}
			return view('admin.team.add', compact( 'staff' ));
	    } else {
	    	return view('admin.error403');
	    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddTeamRequest $request)
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
	        $user                   = Auth::user();
	        $team                   = new Team;
	        $staff_team             = new StaffTeam;
	        $team->name             = $request->name;
            if ( Gate::allows( 'manager' ) )
            {
                $team->type_id      = 1;
            }
            if ( Gate::allows( 'team_leader' ) )
            {
                $team->type_id      = 2;
            }
	        $team->active           = 1;
	        $team->save();
	        $staff_team->staff_id   = $user->id;
	        $staff_team->team_id    = $team->id;     
	        $list_member            = $request->input('member');
            if ( $list_member == NULL )
            {
                return redirect()->back()->with(['flash_level'=>'danger','flash_message_addteam'=>'Fail!! Please add your team member!']);
            } else {
                $staff_team->save();
    	        foreach ( $list_member as $member )
    	        {
    	            $staff_team             = new StaffTeam;
    	            $staff_team->staff_id   = $member;
    	            $staff_team->team_id    = $team->id;
    	            $staff_team->save();
    	        }
    	        return redirect()->route('team.index'); 
            }
	    } else {
	    	return view('admin.error403');
	    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
    		if ( Gate::allows('manager') )
    		{
		        $staff_team = StaffTeam::where( 'team_id', $id );
		        return view('admin.team.staff-team', compact('staff_team'));
		    }
	    } else {
	    	return view('admin.error403');
	    }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
    		$staff_team = StaffTeam::where( 'team_id', $id )->get();
    		if ( Gate::allows('manager') )
    		{		        
		        $staff      = Staff::where( 'active', 1 )
		        					->whereNotIn( 'id', StaffTeam::select( 'staff_id' )->where( 'team_id', $id )->get() )
		        					->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', 2 )->orWhere( 'role_id', 3 )->get() )
		        					->get();		        
		    }
		    if ( Gate::allows('team_leader') )
    		{		        
		        $staff      = Staff::where( 'active', 1 )
                                    ->where( 'department_id', Auth::user()->department_id )
		        					->whereNotIn( 'id', StaffTeam::select( 'staff_id' )->where( 'team_id', $id )->get() )
		        					->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', 4 )->get() )
		        					->get();		        
		    }
		    return view('admin.team.edit', compact( 'staff_team', 'staff' ));
	    } else {
	    	return view('admin.error403');
	    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddTeamRequest $request, $id)
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
	        $team         = Team::find($id);
	        $team->name   = $request->name;
	        $team->save();
	        $list_member  = $request->input('member');
            if ( $list_member == NULL )
            {
                return redirect()->route('team.index');
            } else {
    	        foreach ( $list_member as $member )
    	        {
    	            $staff_team             = new StaffTeam;
    	            $staff_team->staff_id   = $member;
    	            $staff_team->team_id    = $team->id;
    	            $staff_team->save();
    	        }
    	        return redirect()->route('team.index'); 
            }
	    } else {
	    	return view('admin.error403');
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if ( Gate::denies( 'system_admin' ) )
    	{
	        $team 		= Team::findOrFail($id);
	        $team->delete();
	        $staff_team = StaffTeam::where( 'team_id', $id );
	        $staff_team->delete();
	        return redirect()->route('team.index');
	    } else {
	    	return view('admin.error403');
	    }
    }
}
