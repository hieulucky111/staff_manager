<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Staff;
use App\Http\Requests\AddStaffRequest;
use App\Http\Requests\UpdateStaffRequest;
use App\Department;
use App\Level;
use App\Position;
use Hash;
use Gate;
use Auth;
use App\StaffTeam;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            if ( Gate::allows( 'manager' ) )
            {
                $staff = Staff::where( 'active', 1 )
                                ->whereNotIn( 'level_id', Level::select( 'id' )->where( 'role_id', 1 )->orWhere( 'role_id', 2 )->get() )
                                ->get();
            }
            if ( Gate::allows( 'team_leader' ) )
            {
                $staff = Staff::where( 'active', 1 )
                                ->where( 'department_id', Auth::user()->department_id )
                                ->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', 4 )->get() )
                                ->get();
            }
            if ( Gate::allows( 'developer' ) )
            {
                return view( 'admin.error403' );
            }
            return view( 'admin.staff.list', compact( 'staff' ) );
        } else {
            return view( 'admin.error403' );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( Gate::denies( 'developer' ) )
        {
            $department =   Department::where( 'active', 1 )->get();
            if ( Gate::allows( 'system_admin' ) )
            {
                $level      =   Level::where( 'role_id', 2 )
                                        ->orWhere( 'role_id', 3 )
                                        ->where( 'active', 1 )
                                        ->get();
                $position   =   Position::where( 'id', 3 )->where( 'active', 1 )->get();
            }
            if ( Gate::allows( 'manager' ) )
            {
                $level      =   Level::where( 'role_id', 3 )
                                        ->orWhere( 'role_id', 4 )
                                        ->where( 'active', 1 )
                                        ->get();
                $position   =   Position::where( 'active', 1 )->get();
            }
            if ( Gate::allows( 'team_leader' ) )
            {
                $level      =   Level::where( 'role_id', 4 )->where( 'active', 1 )->get();
                $position   =   Position::where( 'active', 1 )->get();
                $department =   Department::where( 'active', 1 )
                                            ->where( 'id', Auth::user()->department_id )
                                            ->get();
            }
            return view( 'admin.staff.add', compact( 'level', 'position', 'department' ) );
        } else {
            return view('admin.error403');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddStaffRequest $request)
    {
        $staff                  = new Staff;
        $staff->email           = $request->email;
        $staff->password        = Hash::make($request->password);
        $staff->name            = $request->name;
        $staff->phone           = $request->phone;
        $staff->birthday        = $request->birthday;
        $staff->position_id     = $request->position;
        $staff->level_id        = $request->level;
        $staff->department_id   = $request->department;
        $staff->active          = $request->active;
        $level                  = Level::select( 'role_id' )->find($request->level);
        $staff_level            = $level->role_id;
        if( Gate::allows( 'role_create', $staff_level ) ) {
            $staff->save();
            return redirect()->route('staff.index');
        } else {
            return view('admin.error403');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            $staff = Staff::where('department_id', $id)
                            ->where( 'id', '!=', Auth::user()->id )
                            ->get();
            if ( Gate::allows( 'developer' ) )
            {
                $staff_team = Staff::whereIn( 'id', StaffTeam::select( 'staff_id' )->whereIn( 'team_id', StaffTeam::select( 'team_id' )->where( 'staff_id', Auth::user()->id )->get() )
                                    ->get() );
                $staff      = Staff::where('department_id', $id)
                                    ->where( 'id', '!=', Auth::user()->id )
                                    ->whereIn( 'level_id', Level::select( 'id' )->where( 'role_id', '!=', 3 )->get() )
                                    ->union($staff_team)
                                    ->get();
            }
            return view('admin.review.list-staff', compact('staff'));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::denies( 'system_admin' ) )
        {            
            $department = Department::where( 'active', 1 )->get();
            $position   = Position::where( 'active', 1 )->get();
            $staff      = Staff::find($id);
            if ( Gate::allows( 'team_leader' ) )
            {
                $position   = Position::where( 'active', 1 )
                                        ->where( 'id', '!=', 3 )
                                        ->get();
                $department = Department::where( 'active', 1 )
                                        ->where( 'id', Auth::user()->department_id )
                                        ->get();
                $level      =   Level::where( 'role_id', 4 )->where( 'active', 1 )->get();
            }
            if ( Gate::allows( 'manager' ) )
            {
                $level      =   Level::where( 'role_id', 3 )
                                        ->orWhere( 'role_id', 4 )
                                        ->where( 'active', 1 )
                                        ->get();
                $position   =   Position::where( 'active', 1 )->get();
            }
            if ( Gate::allows( 'developer' ) )
            {
                return view('admin.error403');
            }
            return view('admin.staff.edit', compact( 'staff', 'level', 'department', 'position' ));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStaffRequest $request, $id)
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            $staff                  = Staff::find($id);
            $staff->email           = $request->email;
            $staff->name            = $request->name;
            $staff->phone           = $request->phone;
            $staff->birthday        = $request->birthday;
            $staff->position_id     = $request->position;
            $staff->level_id        = $request->level;
            $staff->department_id   = $request->department;
            $staff->active          = $request->active;
            $staff->save();
            return redirect()->route('staff.index');
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            $staff = Staff::findOrFail($id);
            $staff->delete();
            return redirect()->route('staff.index');
        } else {
            return view('admin.error403');
        }
    }
}
