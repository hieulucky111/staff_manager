<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;
use App\Staff;
use Auth;
use Gate;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            $reviewer               =   Auth::user();
            $review                 =   new Review;
            $review->staff_id       =   $request->id;
            $review->reviewer_id    =   $reviewer->id;
            $review->point          =   $request->point;
            $review->comment        =   $request->comment;
            $review->active         =   1;
            $review->save();
            return redirect()->back();
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ( Gate::denies( 'system_admin' ) )
        {
            $staff      = Staff::find($id);
            $reviewer   = Auth::user();
            $review     = Review::where( 'staff_id', $id )
                                ->where( 'active', 1 )
                                ->get();
            return view('admin.review.add', compact('review', 'staff', 'reviewer'));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
