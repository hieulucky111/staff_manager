<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Level;
use App\Http\Requests\AddLevelRequest;
use App\Role;
use Gate;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $level = Level::where( 'active', 1 )
                            ->where( 'id', '!=', 6 )
                            ->get();
            return view('admin.level.list', compact( 'level' ));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $role = Role::where( 'active', 1 )
                        ->where( 'name', '!=', 'SystemAdmin' )
                        ->get();
            return view( 'admin.level.add', compact( 'role' ) );
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddLevelRequest $request)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $level          = new Level;
            $level->name    = $request->name;
            $level->role_id = $request->role;
            $level->active  = $request->active;
            $level->save();
            return redirect()->route('level.index'); 
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $level = Level::find($id);
            return view('admin.level.edit', compact('level'));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddLevelRequest $request, $id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $level         = Level::find($id);
            $level->name   = $request->name;
            $level->active = $request->active;
            $level->save();
            return redirect()->route('level.index'); 
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $level = Level::findOrFail($id);
            $level->delete();
            return redirect()->route('level.index');
        } else {
            return view('admin.error403');
        }
    }
}
