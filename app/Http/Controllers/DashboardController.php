<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gate;
use App\Staff;
use App\StaffTeam;
use Auth;
use App\Level;
use App\Team;
use DB;

class DashboardController extends Controller
{
    public function select($department)
    {
        //select query uses count number of team leader and developer belong a department
        $static = DB::table( 'department' )
                ->join( 'staff', 'staff.department_id', '=', 'department.id' )
                ->join( 'level', 'level.id', '=', 'staff.level_id' )
                ->join( 'role', 'role.id', '=', 'level.role_id' )
                ->select( 'role.name as name', DB::raw( 'COUNT(staff.id) as number' ), 'role.id' )
                ->where( 'staff.department_id', $department )
                ->groupBy( 'role.id' )
                ->get();
        return $static;
    }
    public function get_dashboard()
    {
    	if ( Gate::denies('system_admin') )
    	{
    		if ( Gate::allows('developer') )
    		{
                //select staff belong team with this developer
    			$staff = Staff::whereIn( 'id', StaffTeam::select( 'staff_id' )
    													->whereIn( 'team_id', StaffTeam::select( 'team_id' )
    													->where( 'staff_id', Auth::user()->id )
    													->get() )
    													->get() )
    	           										->get();
                //select query uses to calculate average point per team
    			$score = DB::table( 'staff_team' )
    						->join( 'team', 'team.id', '=', 'staff_team.team_id' )
    						->join( 'review', 'review.staff_id', '=', 'staff_team.staff_id' )
    						->select( 'team.name as name', DB::raw( 'AVG(point) as score' ), 'staff_team.team_id' )
    						->groupBy( 'staff_team.team_id' )
    						->get();
    			return view('admin.dashboard.morris', compact('staff', 'score'));
    		}
    		if ( Gate::allows('team_leader') )
    		{
    			$static = self::select(Auth::user()->department_id);
    			return view('admin.dashboard.morris', compact('static'));
    		}
    		if ( Gate::allows('manager') )
    		{
    			$static_PHP      = self::select(1);
    			$static_NET      = self::select(2);
    			$static_Mobile   = self::select(3);
    			$static_Game     = self::select(4);
    			$static_3D       = self::select(5);
    			$static_Cert     = self::select(7);
    			return view('admin.dashboard.morris', compact('static_PHP', 'static_NET', 'static_Mobile', 'static_Game', 'static_3D', 'static_Cert'));
    		}  		
    	} else {
    		return view('admin.error403');
    	}    	
    }
}
