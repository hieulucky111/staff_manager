<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Staff;
use Excel;
use Gate;
use Auth;

class ExportController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function export_staff($department)
    {
        $data = Staff::select( 'id', 'name', 'email', 'phone', 'birthday', 'phone' )
                        ->where( 'active', 1 )
                        ->where( 'department_id', $department )
                        ->get();
        return $data;
    }
    public function export_excel() 
    {
        if ( Gate::denies( 'system_admin' ) )
        {
        	Excel::create( 'Staff', function($excel)
        	{
                if ( Gate::allows( 'manager' ) )
                {
            		$data_PHP     = self::export_staff(1);
                    $data_NET     = self::export_staff(2);
                    $data_Mobile  = self::export_staff(3);
                    $data_Game    = self::export_staff(4);
                    $data_3D      = self::export_staff(5);
                    $data_Cert    = self::export_staff(7);
            		$excel->sheet('Staff|PHP', function($sheet) use($data_PHP) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                    	$sheet->fromArray($data_PHP);
            		});
                    $excel->sheet('Staff|.NET', function($sheet) use($data_NET) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data_NET);
                    });
                    $excel->sheet('Staff|Mobile', function($sheet) use($data_Mobile) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data_Mobile);
                    });
                    $excel->sheet('Staff|Game', function($sheet) use($data_Game) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data_Game);
                    });
                    $excel->sheet('Staff|3D', function($sheet) use($data_3D) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data_3D);
                    });
                    $excel->sheet('Staff|Cert', function($sheet) use($data_Cert) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data_Cert);
                    });
                }
                if ( Gate::allows( 'team_leader' ) )
                {
                    $data = self::export_staff(Auth::user()->department_id);
                    $excel->sheet('Staff Your Department', function($sheet) use($data) {
                        $sheet->setBorder('A1:E1', 'thin');
                        $sheet->setAutoSize(true);
                        $sheet->setColumnFormat(array(
                            'E' => 'dd-mm-yyyy',
                        ));
                        $sheet->cells('A1:E1', function($cells){
                            $cells->setBackground( '#00FFFF' );
                            $cells->setAlignment( 'center' );
                        });
                        $sheet->fromArray($data);
                    });
                }
        	} )->download('xlsx');
        	return redirect()->back();
        } else {
            return view('admin.error403');
        }
    }
}