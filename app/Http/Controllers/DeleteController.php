<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\StaffTeam;
use Gate;

//controller uses delete member in team when manager or leader edit team
class DeleteController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function staff_team_delete($id)
    {
        if ( Gate::allows( 'developer' ) )
        {
            return view('admin.error403');
        }
        if ( Gate::denies( 'system_admin' ) )
        {
    	$staff_team = StaffTeam::where( 'staff_id', $id );
    	$staff_team->delete();
    	return redirect()->back();
        } else {
            return view('admin.error403');
        }
    }
}
