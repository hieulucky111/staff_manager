<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Department;
use App\Http\Requests\AddDepartmentRequest;
use Gate;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $department = Department::where( 'active', 1 )
                                    ->where( 'id', '!=', 6 )
                                    ->get();
            return view('admin.department.list', compact( 'department' ));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            return view('admin.department.add');
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddDepartmentRequest $request)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $department         = new Department;
            $department->name   = $request->name;
            $department->active = $request->active;
            $department->save();
            return redirect()->route('department.index'); 
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $department = Department::find($id);
            return view('admin.department.edit', compact('department'));
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddDepartmentRequest $request, $id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $department         = Department::find($id);
            $department->name   = $request->name;
            $department->active = $request->active;
            $department->save();
            return redirect()->route('department.index'); 
        } else {
            return view('admin.error403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Gate::allows( 'system_admin' ) )
        {
            $department = Department::findOrFail($id);
            $department->delete();
            return redirect()->route('department.index');
        } else {
            return view('admin.error403');
        }
    }
}
