<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Staff;
use App\Review;
use App\Http\Requests\EditUserRequest;
use Hash;

class ProfileController extends Controller
{
    public function get_profile()
    {
    	$staff 		= Auth::user();
    	$review 	= Review::where( 'staff_id', $staff->id )->get();
    	return view( 'admin.user.edit', compact( 'staff', 'review' ) );
    }
    public function post_profile(EditUserRequest $request)
    {
    	$staff                  = Staff::find( Auth::user()->id );
        $staff->password        = Hash::make($request->password);
        $staff->name            = $request->name;
        $staff->phone           = $request->phone;
        $staff->birthday        = $request->birthday;
        $staff->save();
        return redirect()->route('getLogout');
    }
}
