@extends('admin.master')
@section('header','User')
@section('action','Edit')
@section('title','User-Edit')
@section('content')
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{!! route('postProfile') !!}" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label>Staff Email</label>
                                <input class="form-control" name="email" readonly="true" value="{!! $staff->email !!}" />
                                <div class="error">{!! $errors->first('email') !!}</div>
                            </div>
                            <div class="form-group">
                                <label>Staff Password</label>
                                <input class="form-control" type="password" name="password" value="<?php echo Session::get('password'); ?>" />
                                <div class="error">{!! $errors->first('password') !!}</div>
                            </div>
                            <div class="form-group">
                                <label>Staff Name</label>
                                <input class="form-control" name="name" value="{!! $staff->name !!}" />
                                <div class="error">{!! $errors->first('name') !!}</div>
                            </div>
                            <div class="form-group">
                                <label>Staff Phone</label>
                                <input class="form-control" type="number" min="0" name="phone" value="{!! $staff->phone !!}" />
                                <div class="error">{!! $errors->first('phone') !!}</div>
                            </div>
                            <div class="form-group">
                                <label>Staff Birthday</label>
                                <input class="form-control" type="text" id="datepicker" name="birthday" value="{!! $staff->birthday !!}" placeholder="Please Choose Staff Birthday" />
                                <div class="error">{!! $errors->first('birthday') !!}</div>
                            </div>
                            <button type="submit" class="btn btn-default">Edit User</button>
                        </form>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Review List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr align="center">
                                            <th>Number</th>
                                            <th>Reviewer</th>
                                            <th>Point</th>
                                            <th>Comment</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $number = 0; ?>
                                        @foreach ( $review as $rv )
                                            <?php $number += 1; ?>
                                            <tr class="even gradeC" align="center">
                                                <td>{!! $number !!}</td>
                                                <td>{!! $rv->reviewer->name !!}</td>
                                                <td>{!! $rv->point !!}</td>
                                                <td>{!! $rv->comment !!}</td>
                                                <td>{!! $rv->created_at !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
