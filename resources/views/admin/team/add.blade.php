@extends('admin.master')
@section('header','Team')
@section('action','Add')
@section('title','Team-Add')
@section('content') 
    <!-- /.col-lg-12 -->
    
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{!! route('team.store') !!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label>Team Name</label>
                <input class="form-control" name="name" value="{!!old('name')!!}" placeholder="Please Enter Team Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <label>Team Member</label>
                <br>
                <select multiple="multiple" placeholder="Please Choose Your Team Member" id="memberSelect" name="member[]" style="width: 585px; height: 34px;">
                    @foreach ( $staff as $st )
                        <option value="{!! $st->id !!}">{!! $st->name !!}</option>
                    @endforeach
                </select>
            </div>
            @if ( Session::has( 'flash_message_addteam' ) )
                    <div class="alert alert-{!!Session::get( 'flash_level' )!!} ">
                        {!!Session::get( 'flash_message_addteam' )!!}
                    </div>
            @endif
            <button type="submit" class="btn btn-default">Team Add</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
@endsection