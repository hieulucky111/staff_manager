@extends('admin.master')
@section('header','Team')
@section('action','Edit')
@section('title','Team-Edit')
@section('content') 
    <!-- /.col-lg-12 -->    
    <div class="col-lg-7" style="padding-bottom:120px">
    {!! Form::open(array('route'=>array('team.update', $staff_team->first()->team_id),'method'=>'PUT')) !!}
        <form action="" method="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label>Team Name</label>
                <input class="form-control" name="name" value="{!! $staff_team->first()->team->name !!}" placeholder="Please Enter Team Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <label>Team Member</label>
                <br>
                <select multiple="multiple" placeholder="Please Choose Your Team Member" id="memberSelect" name="member[]" style="width: 575px; height: 34px;">
                    @foreach ( $staff as $st )
                        <option value="{!! $st->id !!}">{!! $st->name !!}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-default">Team Edit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    {!! Form::close() !!}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Team Member
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr align="center">
                                    <th>Number</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Birthday</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $number = 0; ?>
                                @foreach ( $staff_team as $st )
                                    <tr class="even gradeC" align="center">
                                        <?php $number += 1; ?>
                                        <td>{!! $number !!}</td>
                                        <td>{!! $st->staff->name !!}</td>
                                        <td>{!! $st->staff->phone !!}</td>
                                        <td>{!! $st->staff->birthday !!}</td>
 
                                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{!! route( 'deleteStaffTeam',$st->staff_id ) !!}"><button onclick="return Confirm('Are you want to delete this staff!')" type="submit" id="delete" class="btn btn-link">Delete</button></a></td>
           
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection