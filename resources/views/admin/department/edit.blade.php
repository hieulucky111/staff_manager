@extends('admin.master')
@section('header','Department')
@section('action','Edit')
@section('title','Department-Edit')
@section('content') 
    <!-- /.col-lg-12 -->
    
    <div class="col-lg-7" style="padding-bottom:120px">
    {!! Form::open(array('route'=>array('department.update',$department->id),'method'=>'PUT')) !!}
        <form action="" method="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label>Department Name</label>
                <input class="form-control" name="name" value="{!! $department->name !!}" placeholder="Please Enter Category Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <?php $check = 'true'; ?>
                <label>Department Active</label>
                @if ( $department->active == 0 )               
                <label class="radio-inline">
                    <input name="active" value="0" checked="<?php echo $check ?>" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" checked="" type="radio">Active
                </label>
                @elseif ( $department->active == 1 )
                <label class="radio-inline">
                    <input name="active" value="0" checked="" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" checked="<?php echo $check ?>" type="radio">Active
                </label>
                @endif
            </div>
            <button type="submit" class="btn btn-default">Department Edit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    {!! Form::close() !!}
    </div>
@endsection