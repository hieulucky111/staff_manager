@extends('admin.master')
@section('header','Level')
@section('action','List')
@section('title','Level-List')
@section('content')
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Level List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr align="center">
                                            <th>Number</th>
                                            <th>Name</th>
                                            <th>Delete</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $number = 0; ?>
                                        @foreach ( $level as $lev )
                                            <?php $number += 1; ?>
                                            <tr class="even gradeC" align="center">
                                                <td>{!! $number !!}</td>
                                                <td>{!! $lev->name !!}</td>
                                                {!! Form::open(array('route'=>array('level.destroy',$lev->id),'method'=>'DELETE')) !!}
                                                    <form action="" method="">
                                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><button onclick="return Confirm('Are you want to delete this Level!')" type="submit" id="delete" class="btn btn-link">Delete</button></td>
                                                    </form>
                                                {!! Form::close() !!}               
                                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! route('level.edit',$lev->id) !!}">Edit</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
