@extends('admin.master')
@section('header','Level')
@section('action','Add')
@section('title','Level-Add')
@section('content') 
    <!-- /.col-lg-12 -->
    
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{!! route('level.store') !!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label>Level Name</label>
                <input class="form-control" name="name" value="{!!old('name')!!}" placeholder="Please Enter Level Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <label>Role</label>
                <select name="role" class="form-control">
                    @foreach ( $role as $r )
                        <option value="{!! $r->id !!}">{!! $r->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Active</label>
                <label class="radio-inline">
                    <input name="active" value="0" checked="" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" type="radio">Active
                </label>
            </div>
            <button type="submit" class="btn btn-default">Level Add</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
@endsection