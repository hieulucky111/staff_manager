@extends('admin.master')
@section('header','Dashboard')
@section('action','')
@section('title','Dashboard')
@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

            <!-- /.row -->
            <div class="row">
                @can( 'developer' )
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Number</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Birthday</th>
                                <th>Position</th>
                                <th>Department</th>
                                <th>Level</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $number = 0; ?>
                            @foreach ( $staff as $st )
                                <tr class="even gradeC" align="center">
                                    <?php $number += 1; ?>
                                    <td>{!! $number !!}</td>
                                    <td>{!! $st->name !!}</td>
                                    <td>{!! $st->phone !!}</td>
                                    <td>{!! $st->birthday !!}</td>
                                    <td>{!! $st->position->name !!}</td>
                                    <td>{!! $st->department->name!!}</td>
                                    <td>{!! $st->level->name !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Number</th>
                                <th>Team</th>
                                <th>AVG Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $number = 0; ?>
                            @foreach ( $score as $sc )
                                <tr class="even gradeC" align="center">
                                    <?php $number += 1; ?>
                                    <td>{!! $number !!}</td>
                                    <td>{!! $sc->name !!}</td>
                                    <td>{!! $sc->score !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endcan
                @can( 'team_leader' )
                <a href="{!! route('exportExcel') !!}">
                    <button type="submit" class="btn btn-default btn-md">
                        <span class="glyphicon glyphicon-file" aria-hidden="true"></span> EXCEL
                    </button>
                </a>
                <br><br>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Your Department 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="team_leader"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                @endcan
                @can( 'manager' )
                <a href="{!! route('exportExcel') !!}">
                    <button type="submit" class="btn btn-default btn-md">
                        <span class="glyphicon glyphicon-file" aria-hidden="true"></span> EXCEL
                    </button>
                </a>
                <br><br>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            PHP 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="PHP"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            .NET 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="NET"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Mobile 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="Mobile"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Game 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="Game"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            3D 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="3D"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Cert 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="Cert"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                @endcan
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        @can( 'team_leader' )
        <?php
            echo "<script>
                    Morris.Donut({
                    element: 'team_leader',
                    data: [";
                    foreach ( $static as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
        ?>
        @endcan
        @can( 'manager' )
        <?php
            echo "<script>
                    Morris.Donut({
                    element: 'PHP',
                    data: [";
                    foreach ( $static_PHP as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
            echo "<script>
                    Morris.Donut({
                    element: 'NET',
                    data: [";
                    foreach ( $static_NET as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
            echo "<script>
                    Morris.Donut({
                    element: 'Mobile',
                    data: [";
                    foreach ( $static_Mobile as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
            echo "<script>
                    Morris.Donut({
                    element: 'Game',
                    data: [";
                    foreach ( $static_Game as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
            echo "<script>
                    Morris.Donut({
                    element: '3D',
                    data: [";
                    foreach ( $static_3D as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
            echo "<script>
                    Morris.Donut({
                    element: 'Cert',
                    data: [";
                    foreach ( $static_Cert as $st )
                    {
                        echo "{label: '$st->name', value: '$st->number' },";
                    }
                    echo "
                    ]
                });     
            </script>";
        ?>
        @endcan
@endsection
