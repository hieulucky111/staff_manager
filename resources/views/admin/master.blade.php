<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Staff_Manager">
    <meta name="author" content="Võ Minh Bảo Hiếu">
    <title>Staff Manager | @yield('title')</title>
    <!--CSS multiple select -->
    <link href="{{ asset('admin/multipleselect/sumoselect.css')}}" rel="stylesheet" />
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('admin/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('admin/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{{ asset('admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="{{ asset('admin/bower_components/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('admin/bower_components/datatables-responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
    <!--Jquery UI -->
    <link rel="stylesheet" href="{{asset('admin/jquery-ui/jquery-ui.min.css')}}">
    <!--CSS-->
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
    <!-- Morris Charts CSS -->
    <link href="{{ asset('admin/bower_components/morrisjs/morris.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! route('getProfile') !!}">{!! Auth::user()->email !!} - Staff Manager</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{!! route('getProfile') !!}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{!! route('getLogout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{!! route('getDashboard') !!}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Staff<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! route('staff.index') !!}">List Staff</a>
                                </li>
                                <li>
                                    <a href="{!! route('staff.create') !!}">Add Staff</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Team<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! route('team.index') !!}">List Team</a>
                                </li>
                                <li>
                                    <a href="{!! route('team.create') !!}">Add Team</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Department<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! route('department.index') !!}">List Department</a>
                                </li>
                                <li>
                                    <a href="{!! route('department.create') !!}">Add Department</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Level<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! route('level.index') !!}">List Level</a>
                                </li>
                                <li>
                                    <a href="{!! route('level.create') !!}">Add Level</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-comments-o fa-fw"></i> Review<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach ( $department as $deps )
                                    <li>
                                        <a href="{!! route('staff.show',$deps->id) !!}">{!! $deps->name !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        {{-- <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">List User</a>
                                </li>
                                <li>
                                    <a href="#">Add User</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li> --}}
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('header')
                            <small>@yield('action')</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-{!!Session::get('flash_level')!!} ">
                                {!!Session::get('flash_message')!!}
                            </div>
                        @endif
                    </div>
                    <!--Đây là nơi chứa nội dung -->
                    @yield('content')
                    <!--Kết thúc nơi chứa nội dung-->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('admin/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('admin/dist/js/sb-admin-2.js')}}"></script>

    <!-- DataTables JavaScript -->
    <script src="{{ asset('admin/bower_components/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>
    <!--Jquery UI -->
    <!--<script type="text/javascript" src="{{asset('admin/jquery-ui/external/jquery/jquery.js')}}"></script>-->
    <script type="text/javascript" src="{{asset('admin/jquery-ui/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/my-js.js')}}"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <!-- Flot Charts JavaScript -->
    <script src="{{ asset('admin/bower_components/flot/excanvas.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('admin/bower_components/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('admin/bower_components/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('admin/bower_components/flot/jquery.flot.time.js')}}"></script>
    <script src="{{ asset('admin/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('admin/js/flot-data.js')}}"></script>
    <!-- Morris Charts JavaScript -->
    
    <!--<script src="{{ asset('admin/bower_components/raphael/raphael-min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/morrisjs/morris.min.js')}}"></script>
    <script src="{{ asset('admin/js/morris-data.js')}}"></script>-->
    <!-- JS multiple select -->
    <script src="{{ asset('admin/multipleselect/jquery.sumoselect.min.js')}}"></script>
    
</body>

</html>
