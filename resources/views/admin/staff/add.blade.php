@extends('admin.master')
@section('header','Staff')
@section('action','Add')
@section('title','Staff-Add')
@section('content') 
    <!-- /.col-lg-12 -->
    
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{!! route('staff.store') !!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label>Department</label>
                <select name="department" class="form-control"> 
                    @foreach ( $department as $deps )
                        <option value="{!! $deps->id !!}">{!! $deps->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Position</label>
                <select name="position" class="form-control">
                    @foreach ( $position as $pos )
                        <option value="{!! $pos->id !!}">{!! $pos->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    @foreach ( $level as $lev )
                        <option value="{!! $lev->id !!}">{!! $lev->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Staff Email</label>
                <input class="form-control" name="email" value="{!!old('email')!!}" placeholder="Please Enter Staff Email" />
                <div class="error">{!! $errors->first('email') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Password</label>
                <input class="form-control" type="password" name="password" value="{!!old('password')!!}" placeholder="Please Enter Staff Password" />
                <div class="error">{!! $errors->first('password') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Name</label>
                <input class="form-control" name="name" value="{!!old('name')!!}" placeholder="Please Enter Staff Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Phone</label>
                <input class="form-control" type="number" min="0" name="phone" value="{!!old('phone')!!}" placeholder="Please Enter Staff Phone" />
                <div class="error">{!! $errors->first('phone') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Birthday</label>
                <input class="form-control" type="text" id="datepicker" name="birthday" value="{!!old('birthday')!!}" placeholder="Please Choose Staff Birthday" />
                <div class="error">{!! $errors->first('birthday') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Active</label>
                <label class="radio-inline">
                    <input name="active" value="0" checked="" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" type="radio">Active
                </label>
            </div>
            <button type="submit" class="btn btn-default">Staff Add</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
@endsection