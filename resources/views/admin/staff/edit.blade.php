@extends('admin.master')
@section('header','Staff')
@section('action','Edit')
@section('title','Staff-Edit')
@section('content') 
    <!-- /.col-lg-12 -->
    
    <div class="col-lg-7" style="padding-bottom:120px">
        {!! Form::open(array('route'=>array('staff.update',$staff->id),'method'=>'PUT')) !!}
            <div class="form-group">
                <label>Department</label>
                <select name="department" class="form-control">
                    <option value="{!! $staff->department->id !!}">{!! $staff->department->name !!}</option>
                    @foreach ( $department as $deps )
                        <option value="{!! $deps->id !!}">{!! $deps->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Position</label>
                <select name="position" class="form-control">
                    <option value="{!! $staff->position->id !!}">{!! $staff->position->name !!}</option>
                    @foreach ( $position as $pos )
                        <option value="{!! $pos->id !!}">{!! $pos->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    <option value="{!! $staff->level->id !!}">{!! $staff->level->name !!}</option>
                    @foreach ( $level as $lev )
                        <option value="{!! $lev->id !!}">{!! $lev->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Staff Email</label>
                <input class="form-control" name="email" value="{!! $staff->email !!}" readonly="true" />
                <div class="error">{!! $errors->first('email') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Name</label>
                <input class="form-control" name="name" value="{!! $staff->name !!}" placeholder="Please Enter Category Name" />
                <div class="error">{!! $errors->first('name') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Phone</label>
                <input class="form-control" type="number" min="0" name="phone" value="{!! $staff->phone !!}" placeholder="Please Enter Category Order" />
                <div class="error">{!! $errors->first('phone') !!}</div>
            </div>
            <div class="form-group">
                <label>Staff Birthday</label>
                <input class="form-control" type="text" id="datepicker" name="birthday" value="{!! $staff->birthday !!}" placeholder="Please Enter Category Keywords" />
                <div class="error">{!! $errors->first('birthday') !!}</div>
            </div>
            <div class="form-group">
            	<?php $check = 'true'; ?>
            	<label>Staff Active</label>
            	@if ( $staff->active == 0 )               
                <label class="radio-inline">
                    <input name="active" value="0" checked="<?php echo $check ?>" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" checked="" type="radio">Active
                </label>
                @elseif ( $staff->active == 1 )
                <label class="radio-inline">
                    <input name="active" value="0" checked="" type="radio">Deactive
                </label>
                <label class="radio-inline">
                    <input name="active" value="1" checked="<?php echo $check ?>" type="radio">Active
                </label>
                @endif
            </div>
            <button type="submit" class="btn btn-default">Staff Edit</button>
            <button type="reset" class="btn btn-default">Reset</button>
        {!! Form::close() !!}
    </div>
@endsection