@extends('admin.master')
@section('header','Staff')
@section('action','List')
@section('title','Staff-List')
@section('content')
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Staff List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr align="center">
                                            <th>Number</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Birthday</th>
                                            <th>Position</th>
                                            <th>Department</th>
                                            <th>Level</th>
                                            <th>Delete</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $number = 0; ?>
                                        @foreach ( $staff as $st )
                                            <tr class="even gradeC" align="center">
                                                <?php $number += 1; ?>
                                                <td>{!! $number !!}</td>
                                                <td>{!! $st->name !!}</td>
                                                <td>{!! $st->phone !!}</td>
                                                <td>{!! $st->birthday !!}</td>
                                                <td>{!! $st->position->name !!}</td>
                                                <td>{!! $st->department->name!!}</td>
                                                <td>{!! $st->level->name !!}</td>
                                                {!! Form::open(array('route'=>array('staff.destroy',$st->id),'method'=>'DELETE')) !!}
                                                    <form action="" method="">
                                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><button onclick="return Confirm('Are you want to delete this staff!')" type="submit" id="delete" class="btn btn-link">Delete</button></td>
                                                    </form>
                                                {!! Form::close() !!}               
                                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! route('staff.edit',$st->id) !!}">Edit</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
