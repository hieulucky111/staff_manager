@extends('admin.master')
@section('header','Staff')
@section('action','List')
@section('title','Staff-List')
@section('content')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Staff Review
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr align="center">
                                    <th>Number</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Birthday</th>
                                    <th>Position</th>
                                    <th>Level</th>
                                    <th>Review</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $number = 0; ?>
                                @foreach ( $staff as $st )                                   
                                        <tr class="even gradeC" align="center">
                                            <?php $number += 1; ?>
                                            <td>{!! $number !!}</td>
                                            <td>{!! $st->name !!}</td>
                                            <td>{!! $st->phone !!}</td>
                                            <td>{!! $st->birthday !!}</td>
                                            <td>{!! $st->position->name !!}</td>
                                            <td>{!! $st->level->name !!}</td>
                                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! route('review.show',$st->id) !!}">Review</a></td>
                                        </tr>                                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
