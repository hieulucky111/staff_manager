@extends('admin.master')
@section('header','Review')
@section('action','Add')
@section('title','Review-Add')
@section('content')
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{!! route('review.store') !!}" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label>Staff ID</label>
                                <input class="form-control" name="id" readonly="true" value="{!! $staff->id !!}" />
                            </div>
                            <div class="form-group">
                                <label>Staff Email</label>
                                <input class="form-control" name="email" readonly="true" value="{!! $staff->email !!}" />
                            </div>
                            <div class="form-group">
                                <label>Staff Name</label>
                                <input class="form-control" name="name" readonly="true" value="{!! $staff->name !!}" />
                            </div>
                            <div class="form-group">
                                <label>Staff Phone</label>
                                <input class="form-control" type="number" min="0" name="phone" readonly="true" value="{!! $staff->phone !!}" />
                            </div>
                            <div class="form-group">
                                <label>Staff Birthday</label>
                                <input class="form-control" type="text" name="birthday" readonly="true" value="{!! $staff->birthday !!}" />
                            </div>
                            <div class="form-group">
                                <label>Reviewer Name</label>
                                <input class="form-control" type="text" min="0" name="reviewer" readonly="true" value="{!! $reviewer->name !!}" />
                            </div>
                            <div class="form-group">
                                <label>Point</label>
                                <input class="form-control" type="range" onchange="updateTextInput(this.value);" min="0" max="10" name="point" placeholder="Please Enter Staff Phone" />
                                <input class="form-control" type="text" readonly="true" id="rangeValue" value="0">
                            </div>
                            <div class="form-group">
                                <label>Reviewer Comment</label>
                                <input class="form-control" type="text" name="comment" value="" />
                            </div>
                            <button type="submit" class="btn btn-default">Review Add</button>
                        </form>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Review List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr align="center">
                                            <th>Number</th>
                                            <th>Reviewer</th>
                                            <th>Point</th>
                                            <th>Comment</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $number = 0; ?>
                                        @foreach ( $review as $rv )
                                            <?php $number += 1; ?>
                                            <tr class="even gradeC" align="center">
                                                <td>{!! $number !!}</td>
                                                <td>{!! $rv->reviewer->name !!}</td>
                                                <td>{!! $rv->point !!}</td>
                                                <td>{!! $rv->comment !!}</td>
                                                <td>{!! $rv->created_at !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
